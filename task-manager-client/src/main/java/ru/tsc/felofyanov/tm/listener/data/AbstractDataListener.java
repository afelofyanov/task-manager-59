package ru.tsc.felofyanov.tm.listener.data;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.felofyanov.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;
}
