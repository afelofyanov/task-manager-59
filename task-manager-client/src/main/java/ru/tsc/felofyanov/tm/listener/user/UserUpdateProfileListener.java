package ru.tsc.felofyanov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.UserUpdateProfileRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getName() {
        return "user-update-profile";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update profile of current user";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE USER PROFILE]");

        System.out.println("ENTER NEW FIRST NAME");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW MIDDLE NAME");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW LAST NAME");
        @NotNull final String lastName = TerminalUtil.nextLine();

        @NotNull final UserUpdateProfileRequest request =
                new UserUpdateProfileRequest(getToken(), firstName, middleName, lastName);
        getUserEndpoint().updateProfileUser(request);
    }
}
