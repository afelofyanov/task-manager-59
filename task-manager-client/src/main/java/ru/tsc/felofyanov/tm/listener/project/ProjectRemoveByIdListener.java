package ru.tsc.felofyanov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        getProjectEndpoint().removeProjectById(request);
    }
}
