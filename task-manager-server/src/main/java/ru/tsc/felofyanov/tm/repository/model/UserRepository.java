package ru.tsc.felofyanov.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.api.repository.model.IUserRepository;
import ru.tsc.felofyanov.tm.model.User;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Getter
@Repository
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository() {
        super(User.class);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable String login) {
        final TypedQuery<User> query =
                entityManager.createQuery("FROM " + getModelName() + " WHERE login = :login", clazz)
                        .setHint("org.hibernate.cacheable", true)
                        .setParameter("login", login);
        @NotNull final List<User> result = query.getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable String email) {
        final TypedQuery<User> query =
                entityManager.createQuery("FROM " + getModelName() + " WHERE email = :email", clazz)
                        .setHint("org.hibernate.cacheable", true)
                        .setParameter("email", email);
        @NotNull final List<User> result = query.getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public User removeByLogin(@Nullable String login) {
        @Nullable final Optional<User> model = Optional.ofNullable(findByLogin(login));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }
}
