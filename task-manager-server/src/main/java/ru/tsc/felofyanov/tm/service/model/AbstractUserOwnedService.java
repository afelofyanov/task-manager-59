package ru.tsc.felofyanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.model.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.api.repository.model.IUserRepository;
import ru.tsc.felofyanov.tm.api.service.model.IUserOwnerService;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ModelEmptyException;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.AbstractWbs;
import ru.tsc.felofyanov.tm.model.User;

import java.util.List;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractWbs, R extends IUserOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnerService<M> {

    @NotNull
    @Autowired
    protected IUserRepository userRepository;

    @NotNull
    protected abstract IUserOwnerRepository<M> getRepository();

    @Nullable
    @Override
    @Transactional
    public M create(@Nullable final User user, @Nullable final String name) {
        if (user == null) throw new UserNotFoundException();
        return createByUserId(user.getId(), name);
    }

    @Nullable
    @Override
    @Transactional
    public M create(@Nullable final User user, @Nullable final String name, @Nullable final String description) {
        if (user == null) throw new UserNotFoundException();
        return createByUserId(user.getId(), name, description);
    }

    @Nullable
    @Override
    @Transactional
    public M createByUserId(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        return repository.create(userRepository.findOneById(userId), name);
    }

    @Nullable
    @Override
    @Transactional
    public M createByUserId(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        return repository.create(userRepository.findOneById(userId), name, description);
    }

    @NotNull
    @Override
    public List<M> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        repository.clearByUserId(userId);
    }

    @Override
    public boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneByIdUserId(userId, id) != null;
    }

    @Nullable
    @Override
    public M findOneByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        return repository.findOneByIdUserId(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndexByUserId(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        return repository.findOneByIndexByUserId(userId, index);
    }

    @Nullable
    @Override
    @Transactional
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        @Nullable final M result = repository.remove(userId, model);
        if (result == null) throw new ModelEmptyException();
        return result;
    }

    @Nullable
    @Override
    @Transactional
    public M removeByIdByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        @Nullable final M result = repository.removeByIdByUserId(userId, id);
        if (result == null) throw new ModelEmptyException();
        return result;
    }

    @Nullable
    @Override
    @Transactional
    public M removeByIndexByUserId(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        @Nullable final M result = repository.removeByIndexByUserId(userId, index);
        if (result == null) throw new ModelEmptyException();
        return result;
    }

    @Override
    @Transactional
    public M update(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        return repository.update(model);
    }

    @NotNull
    @Override
    @Transactional
    public M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        @Nullable final M result;
        result = repository.findOneByIdUserId(userId, id);
        if (result == null) throw new ModelNotFoundException();

        result.setName(name);
        result.setDescription(description);
        repository.update(result);
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public M updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        @Nullable final M result;

        result = repository.findOneByIndexByUserId(userId, index);
        if (result == null) throw new ModelNotFoundException();
        result.setName(name);
        result.setDescription(description);
        return repository.update(result);
    }

    @NotNull
    @Override
    @Transactional
    public M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        @Nullable final M result;

        result = repository.findOneByIdUserId(userId, id);
        if (result == null) throw new ModelNotFoundException();
        result.setStatus(status);
        return repository.update(result);
    }

    @NotNull
    @Override
    @Transactional
    public M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        @Nullable final M result;

        result = repository.findOneByIndexByUserId(userId, index);
        if (result == null) throw new ModelNotFoundException();
        result.setStatus(status);
        return repository.update(result);
    }

    @Override
    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnerRepository<M> repository = getRepository();
        return repository.countByUserId(userId);
    }
}
