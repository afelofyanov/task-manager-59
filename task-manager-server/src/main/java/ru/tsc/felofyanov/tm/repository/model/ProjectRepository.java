package ru.tsc.felofyanov.tm.repository.model;

import lombok.Getter;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.api.repository.model.IProjectRepository;
import ru.tsc.felofyanov.tm.model.Project;

@Getter
@Repository
public class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
        super(Project.class);
    }
}
